#!/bin/env sh
## editor y navegador por defecto
mvcEditor="code"
mvcBrowser="brave-browser"
## lo unico que da la opcion de cambiar es el nombre del proyecto
mvcName="newName"
mvcHost="https://localhost:5001/"

echo "Asegurese de ejecutar este script en la carpeta donde desea crear el proyecto."
echo "Ingrese el nuevo nombre de su proyecto: "
read mvcName
dotnet new mvc -o ${mvcName}

cd ${mvcName}

## se crea fichero
echo "#!/bin/env sh" > ${mvcName}Server.sh
echo "" >> ${mvcName}Server.sh
echo "cd .." >> ${mvcName}Server.sh
echo "pkill ${mvcName}" >> ${mvcName}Server.sh
echo "pkill dotnet" >> ${mvcName}Server.sh
echo "finished=true" >> ${mvcName}Server.sh
echo "echo 'Abrir ${mvcEditor} y ${mvcBrowser}? y/N'" >> ${mvcName}Server.sh
echo "read read" >> ${mvcName}Server.sh
echo "if [ $read = 'y' ]" >> ${mvcName}Server.sh
echo "then" >> ${mvcName}Server.sh
echo "   ${mvcEditor} -r ${mvcName} &" >> ${mvcName}Server.sh
echo "   ${mvcBrowser} ${mvcHost} &" >> ${mvcName}Server.sh
echo "fi" >> ${mvcName}Server.sh
echo "while [ \$finished ]" >> ${mvcName}Server.sh
echo "do" >> ${mvcName}Server.sh
echo "   echo ''" >> ${mvcName}Server.sh
echo "   echo '- Preciona <enter> para recompílar'" >> ${mvcName}Server.sh
echo "   echo '- Preciona <q> y <enter> para finalizar'" >> ${mvcName}Server.sh
echo "   echo ''" >> ${mvcName}Server.sh
echo "   dotnet run --project ${mvcName} &" >> ${mvcName}Server.sh
echo "   echo 'recargando...'" >> ${mvcName}Server.sh
echo "   read reload" >> ${mvcName}Server.sh
echo "   pkill ${mvcName}" >> ${mvcName}Server.sh
echo "pkill dotnet" >> ${mvcName}Server.sh
echo "   if [ \${reload} = 'q' ]" >> ${mvcName}Server.sh
echo "   then" >> ${mvcName}Server.sh
echo "     echo 'Se preciono q, adios!'" >> ${mvcName}Server.sh
echo "     finished=false" >> ${mvcName}Server.sh
echo "     break;" >> ${mvcName}Server.sh
echo "   fi" >> ${mvcName}Server.sh
echo "done" >> ${mvcName}Server.sh
echo "pkill ${mvcName}" >> ${mvcName}Server.sh
echo "pkill dotnet" >> ${mvcName}Server.sh

chmod +x ./${mvcName}Server.sh
echo "Se creo el fichero ${mvcName}Server.sh para que lo ejecute y automatice el proceso de recarga, asegurate de siempre ejecutarlo dentro de una terminal y no en segundo plano..."
echo "Ejecutar ahora? y/N"
read read
if [ $read = 'y' ]
then
    sh -x "./${mvcName}Server.sh"
fi
