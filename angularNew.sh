#!/bin/env sh
angularEditor="code"
angularBrowser="brave-browser"
angularName="newName"
angularHost="http://localhost:4200/"

echo "Asegurese de ejecutar este script en la carpeta donde desea crear el proyecto."
echo "Ingrese el nuevo nombre de su proyecto: "
read angularName
ng new ${angularName} --directory ${angularName}

cd ${angularName}

## se crea fichero
echo "#!/bin/env sh" > ${angularName}Server.sh
echo "" >> ${angularName}Server.sh
echo "pkill ${angularName}" >> ${angularName}Server.sh
echo "pkill ng" >> ${angularName}Server.sh
echo "finished=true" >> ${angularName}Server.sh
echo "echo 'Abrir ${angularEditor} y ${angularBrowser}? y/N'" >> ${angularName}Server.sh
echo "read read" >> ${angularName}Server.sh
echo "if [ \$read = 'y' ]" >> ${angularName}Server.sh
echo "then" >> ${angularName}Server.sh
echo "   ${angularBrowser} ${angularHost} &" >> ${angularName}Server.sh
echo "   ${angularEditor} -r ${angularName} &" >> ${angularName}Server.sh
echo "fi" >> ${angularName}Server.sh
echo "while [ \$finished ]" >> ${angularName}Server.sh
echo "do" >> ${angularName}Server.sh
echo "   echo ''" >> ${angularName}Server.sh
echo "   echo '- Preciona <enter> para recompílar'" >> ${angularName}Server.sh
echo "   echo '- Preciona <c> y <enter> para nuevo componente'" >> ${angularName}Server.sh
echo "   echo '- Preciona <q> y <enter> para finalizar'" >> ${angularName}Server.sh
echo "   echo ''" >> ${angularName}Server.sh
echo "   ng serve &" >> ${angularName}Server.sh
echo "   echo 'recargando...'" >> ${angularName}Server.sh
echo "   read reload" >> ${angularName}Server.sh
echo "   pkill ${angularName}" >> ${angularName}Server.sh
echo "   pkill ng" >> ${angularName}Server.sh
echo "   if [ \${reload} = 'q' ]" >> ${angularName}Server.sh
echo "   then" >> ${angularName}Server.sh
echo "     finished=false" >> ${angularName}Server.sh
echo "     break;" >> ${angularName}Server.sh
echo "   elif [ \${reload} = 'c' ]" >> ${angularName}Server.sh
echo "   then" >> ${angularName}Server.sh
echo "     ng generate component ${componentName}" >> ${angularName}Server.sh
echo "   fi" >> ${angularName}Server.sh
echo "done" >> ${angularName}Server.sh
echo "pkill ${angularName}" >> ${angularName}Server.sh
echo "pkill angular" >> ${angularName}Server.sh
echo "pkill ng" >> ${angularName}Server.sh

chmod +x ./${angularName}Server.sh
echo "Se creo el fichero ${angularName}Server.sh para que lo ejecute y automatice el proceso de recarga, asegurate de siempre ejecutarlo dentro de una terminal y no en segundo plano..."
echo "Ejecutar ahora? y/N"
read read
if [ $read = 'y' ]
then
    sh -x "./${angularName}Server.sh"
fi
