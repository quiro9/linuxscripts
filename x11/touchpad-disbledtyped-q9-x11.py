#!/usr/bin/python

## touchpad-disabledtyped-q9 esta licenciado bajo GNU/GPL v3, realizado por quiro9.
## puede modificarlo libremente bajo los terminos de dicha licencia más información: http://www.gnu.org/licenses/gpl-3.0.html .

## este script fue pensado para desactivar el touchpad al escribir en sistemas X: 
## la idea original es evitar toques accidentales del touchpad al momento de escribir (no entorpecer la experiencia de usuario)
##
## lee pulsaciones de tecla y si se detectan desactiva el touchpad los MS configurados,
## tambien se puede asociar eventos a las teclas que se precionanan esto sirve por si
## tu escritorio no reconoce o simplemente no compatibiliza atajos como teclas de acceso.
## pueden asignarse comandos personalizados.

## IMPORTANTE: Necesitas instalar pyxhook, puedes hacer:
## sudo pip install pyxhook

import os, time, pyxhook, threading

## NOMBRE de touchpad (ver con xinput o xev, SE UTILIZA SI SE DESACTIVA TOUCHPAD)
touchpad_name = "ELAN1200:00 04F3:3045 Touchpad"
## tiempo por defecto que desactiva touchpad (y control de bucle) -> default: 0.3 (300ms)
touchpad_disable_sec = 0.3

######################################################################
## evitar modificar de aqui en adelante (si no se sabe que se hace) ##
######################################################################
is_null = 0
def OnKeyPress(event):
	global touchpadOn, is_null

	if touchpad_disable_sec > 0.0:
           	## cancela el hilo actual de activación de touchpad
		print ("-- se preciona: " + str(event.Ascii) + " -> " + str(event.Key))
		print ("-- evento: " + str(event.MessageName))
		if event.Key:
			touchpadOn.cancel()
			## ACA SE PUEDE COLOCAR PRESIONES DE TECLA PERSONALIZADAS a comandos os. o sys. (opcional) .
			## si se presiona <alt> <ctrl> <tab> o <super> se ignora
			if event.MessageName == "key down":
				if (event.Ascii == 225 or event.Ascii == 226 or event.Ascii == 227 or event.Ascii == 228 or event.Ascii == 233 or event.Ascii == 235 or event.Ascii == 9):
					is_null += 1
				if is_null == 0:
					TouchpadControlOff()
			
			if event.MessageName == "key up":
				if  (event.Ascii == 225 or event.Ascii == 226 or event.Ascii == 227 or event.Ascii == 228 or event.Ascii == 233 or event.Ascii == 235 or event.Ascii == 9):
					is_null -= 1
				if is_null == 0:
					touchpadOn = threading.Timer(touchpad_disable_sec, TouchpadControlOn)
					touchpadOn.start()
		if is_null < 0:
			is_null = 0

def TouchpadControlOff():
		print ("-- se desactiva touchpad")
		#os.system("xinput set-prop '" + touchpad_name + "' 'Device Enabled' 0")
		## si en gnome no funciona puedes usar desactivar toques
		os.system("gsettings set org.gnome.desktop.peripherals.touchpad tap-to-click false")
		#synclient TapButton1=0
				
def TouchpadControlOn():
		print ("-- se activa touchpad")
		#os.system("xinput set-prop '" + touchpad_name + "' 'Device Enabled' 1")
		os.system("gsettings set org.gnome.desktop.peripherals.touchpad tap-to-click true")
		#synclient TapButton1=1
		touchpadOn.cancel()

# configuraciones
print("Tiempo de desactivado: " + str(touchpad_disable_sec) +"s [ " + str(int(touchpad_disable_sec*1000)) + "ms ]")
## vars controles se crea un hilo de ejecución que repite para activar el touchpad denuevo
touchpadOn = threading.Timer(touchpad_disable_sec, TouchpadControlOn)
touchpadOn.start()

## escuchar todas las pulsaciones evento pyhookX
new_hook=pyxhook.HookManager()
new_hook.KeyDown = OnKeyPress
new_hook.KeyUp = OnKeyPress
new_hook.HookKeyboard()
new_hook.start()
#fob.close()
#new_hook.cancel()


